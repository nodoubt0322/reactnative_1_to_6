import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { Header, Card, ListItem, Button } from 'react-native-elements'
import Footer from './components/Footer'

const users = [
    {
        name: 'brynn',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg'
    },
    {
        name: 'brynn',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg'
    }
]



export default class App extends React.Component {

    render () {
        return (
            <View style={styles.container}>

                <Header
                    leftComponent={{ icon: 'menu', color: '#fff' }}
                    centerComponent={{ text: 'Daniel app', style: { color: '#fff' } }}
                    rightComponent={{ icon: 'home', color: '#fff' }}
                />
                
                <Card containerStyle={{padding: 0}} >
                    {
                        users.map((u, i) => {
                            return (
                                <ListItem
                                    key={i}
                                    roundAvatar
                                    title={u.name}
                                    avatar={{uri: u.avatar}}
                                />
                            )
                        })
                    }
                </Card>
                
                <Footer />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height:"100%",
        justifyContent: 'space-between'

    }
})
