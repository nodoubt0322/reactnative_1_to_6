
## `Install`

- [create-react-native-app](https://facebook.github.io/react-native/docs/getting-started.html)
- [expo tool](https://expo.io/tools)
  - [expo xde](https://docs.expo.io/versions/latest/introduction/installation)
  - expo Client
  - expo cli
  - expo snack
  - expo sdk
- [What is the best UI Kit for react native?](https://www.quora.com/What-is-the-best-UI-Kit-for-react-native)
- [react-native-elements](https://github.com/react-native-training/react-native-elements)

<br><br>

---

## `Get started`

```
  create-react-native-app AwesomeProject
  cd AwesomeProject
  npm start
  or run app with expo XDE
```

<br><br>

---
## `element` 

| react native  | html          | 
| ------------- |-------------  |
| \<View>       | \<div>        |
| \<Text>       | \<p>          | 
| \<Image>      | \<img>        |



```css
import { Text, View, Image } from 'react-native'


<View style= {{ backgroundColor:'#000', height:200 }} >	
    <Text> Hello world! </Text>
    <Image source={pic} style={{width: 193, height: 110}}/>
</View>

```
<br><br>


---
## `style` 

```jsx
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default class App extends React.Component {
    render () {
        return (
            <View style={styles.container}>
            //<View style= {{ backgroundColor:'#000', height:200 }} >
                <Text> Hello world! </Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
		backgroundColor: '#000',
		height:600
    }
})

```

- 有單位px的只寫數字，其他都是string，ex: width: 20, width: '100%'
- background-color => backgroundColor
- 這裡好幾個坑? 
    - \<View>不能設定color? 
    - position:fixed不能用? 
    - 預設排版是flex
    - and more....?

<br><br>

---
## `register a component`

```jsx
import React, { Component } from 'react';
import { AppRegistry, Image } from 'react-native';

export default class App extends Component {
  render() {
    let pic = {
      uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
    };
    return (
      <Image source={pic} style={{width: 193, height: 110}}/>
    );
  }
}

// skip this line if using Create React Native App
AppRegistry.registerComponent('AwesomeProject', () => App);
```

- react native會自動把App.js當成是root，不管class名稱取成什麼
- 使用create-react-native-app的話, 可以把AppRegistry省略掉

<br><br>

---
## `react-native-elements`

[https://react-native-training.github.io/react-native-elements/docs/header.html](https://react-native-training.github.io/react-native-elements/docs/header.html)

> 加入Header跟Card 

<br><br>

---
## `寫weex比較爽?`

-  [首个使用 Weex 和 Vue 开发的 Hacker News 原生应用](https://juejin.im/entry/5ae7e192f265da0b8c24c053)  
-  [github](https://github.com/weexteam/weex-hackernews/blob/master/src/views/CommentView.vue)
